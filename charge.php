<?php

/**************  AGILE CRM  **//*************************/
define( 'AGILE_DOMAIN', '' );  # Example : define("domain","jim");
define( 'AGILE_USER_EMAIL', 'navjyot@armentum.biz' );
define( 'AGILE_REST_API_KEY', '4en8s33k533f7c4f83pjsa0pc' );

function curl_wrap( $entity, $data, $method, $content_type ) {
	if ( $content_type == null ) {
			$content_type = 'application/json';
	}

	$agile_url = 'https://' . AGILE_DOMAIN . '.agilecrm.com/dev/api/' . $entity;

	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
	curl_setopt( $ch, CURLOPT_UNRESTRICTED_AUTH, true );
	switch ( $method ) {
		case 'POST':
				$url = $agile_url;
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
			break;
		case 'GET':
				$url = $agile_url;
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
			break;
		case 'PUT':
				$url = $agile_url;
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
			break;
		case 'DELETE':
				$url = $agile_url;
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
			break;
		default:
			break;
	}
	curl_setopt(
		$ch, CURLOPT_HTTPHEADER, array(
			"Content-type : $content_type;",
			'Accept : application/json',
		)
	);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_USERPWD, AGILE_USER_EMAIL . ':' . AGILE_REST_API_KEY );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 120 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
	$output = curl_exec( $ch );
	curl_close( $ch );
	return $output;
}


/*****************************AGILE CRM ENDS ****************/


header( 'Access-Control-Allow-Origin: *' );

//let's say each article costs 15.00 bucks

try {
	require_once( './config.php' );

	$token  = $_POST['stripeToken'];
	$email  = $_POST['stripeEmail'];

	$customer = \Stripe\Customer::create(
		array(
			'email' => $email,
			'source'  => $token,
		)
	);

	$charge = \Stripe\Charge::create(
		array(
			'customer' => $customer->id,
			'amount'   => 9900,
			'currency' => 'usd',
		)
	);

	echo '<h1>Successful!!</h1>';
	header( 'Refresh:3; url=index.php' );

	//you can send the file to this email:

} //catch the errors in any way you like

catch ( Stripe_CardError $e ) {

} catch ( Stripe_InvalidRequestError $e ) {
	// Invalid parameters were supplied to Stripe's API

} catch ( Stripe_AuthenticationError $e ) {
	// Authentication with Stripe's API failed
	// (maybe you changed API keys recently)

} catch ( Stripe_ApiConnectionError $e ) {
	// Network communication with Stripe failed
} catch ( Stripe_Error $e ) {

	// Display a very generic error to the user, and maybe send
	// yourself an email
} catch ( Exception $e ) {

	// Something else happened, completely unrelated to Stripe
}

