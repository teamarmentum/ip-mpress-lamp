<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/png" href="assets/images/mpress-logo-final.png" />

	<?php
		header( 'Access-Control-Allow-Origin: *' );
	?>
	<title>mPress - Ctrl.biz</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="description" content="">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<!-- OPEN SANS Font Family -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">

	<!-- Arimo Font Family -->
	<link href="https://fonts.googleapis.com/css?family=Arimo" rel="stylesheet">

	<!-- Abril Fatface Font Family -->
	<link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">

	<!-- CSS Stylesheet -->
	<link rel="stylesheet" href="assets/css/styles.css">
	<link rel="stylesheet" href="assets/css/p-style.css">
	<link rel="stylesheet" href="assets/css/section-3-styles.css">

	<!-- Custom JS -->
	<script src="assets/js/index.js"></script>

	<!-- AGILE CRM -->
	<script id="_agile_min_js" async type="text/javascript" src="https://d1gwclp1pmzk26.cloudfront.net/agile/agile-cloud.js"></script>
	<script type="text/javascript" >
		var Agile_API = Agile_API || {}; Agile_API.on_after_load = function(){
		_agile.set_account('4en8s33k533f7c4f83pjsa0pc', 'curatedtalent');
		_agile.track_page_view();
		_agile_execute_web_rules();};
	</script>
</head>
<body>
	<!-- SECTION 1 -->
	<div class="container-fluid section-1">
		<div class="row header">
			<span>
				<a href="index.php"><img src="assets/images/logo_new.png" class="logo" alt="Logo" /></a>
			</span>
		</div>

		<div class="section-1-main row text-left">
			<div class="col-md-6 col-sm-6">
				<h1 class="section-1-h1"> The World Is Going Mobile. Are You? </h1>
				<p class="section-1-p"> Convert your WordPress website into a native mobile application & increase your revenue
					by 40%. </p>
				<div id="section-1-button-div">
						<button type="button" class="stripe-button-el" id="smooth-scroll-button" style="visibility: visible;">
							<span style="display: block; min-height: 30px;">Add yourself to waiting list</span>
						</button>
					</div>
				<div class="section-1-div"><img src="assets/images/arrownew.png" alt="Arrow" class="section-1-arrow" /><span class="section-1-arrow-text"> 2252 People Already Have!  </span></div>
			</div>
			<div class="col-md-6 col-sm-6" >
				<div class="col-sm-12">
					<img src="assets/images/banner-image.png" alt="Mobiles" class="section-1-mobile2" />
				</div>
			</div>
		</div>
	</div>
	<br/>
	<!-- Video Section-->
	<div class="container-fluid video-section">
		<div class="row">

			<div class="col-md-6 col-md-offset-3 text-center">
				<h1> A promo always helps... </h1>
			</div>
			<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1  video-bg">
				<video height="auto" width="100%"  loop id="video" controls >
				<source src="assets/video/mpressvideo.mp4" alt="Video" class="img-responsive" />
			</div>
		</div>
	</div>

	<div class="div-video-bg">
	</div>

	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-center video-section-h2">
		<h2 class="h2look"> Want to see how your mobile application is going to look </h2>
	</div>

	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-center video-section-form">
		<div class="col-md-7 col-sm-7">
			<input type="text" placeholder="Provide your email" id="video-email"/>
		</div>
		<div class="col-md-5 col-sm-5">
			<button type="button" class="btn btn-primary" onclick="apiCall()">Join the Waitlist! </button>
		</div>
	</div>


		</div>
	</div>

	<!-- SECTION 2 -->
	<div class="container-fluid section-2">

		<div class="row">
			<div class="col-md-12 text-center">
				<h1 class="section-2-h1"> Interested in numbers? Calculate yourself.</h1>
			</div>
			<div class="col-md-6 col-sm-6">
					<img src="assets/images/stats.png" alt="Stats" class="stats img-responsive" />
			</div>
			<div class="col-md-6 col-sm-6">
				<form action="" method="POST" class="form">
					<div class="row calc">
						<div class="col-sm-6">
							<div class="form-group odd-form-group">
								<label for="">Average daily users</label>
								<input type="text" class="form-control" id="users" placeholder="no. of users" value="5000" onchange="calculateRevenue()">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group even-form-group">
								<label for="">Average minutes per session</label>
								<input type="text" class="form-control" id="mins" placeholder="average mins" value="2" onchange="calculateRevenue()">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group odd-form-group">
								<label for="">Ad impression per minute</label>
								<input type="text" class="form-control" id="ad" placeholder="Ad impression" value="2" onchange="calculateRevenue()">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group even-form-group">
								<label for="">Ad network fill rate (%) </label>
								<input type="text" class="form-control" id="fill_rate" placeholder="enter %" value="80" onchange="calculateRevenue()">
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group odd-form-group">
								<label for="">eCPM ($)</label>
								<input type="text" class="form-control" id="ecpm" placeholder="enter eCPM" value="2" onchange="calculateRevenue()">
							</div>
						</div>
						<br/>
						<br/>
						<div class="col-sm-12 text-center section-2-bottom"><div class="row">
							<div class="col-sm-2 line-left"> </div>
							<div class="col-sm-8 mid-content">
								<p class="section-2-p"><i> Your daily revenue will be around </i></p>
							</div>
							<div class="col-sm-2 line-right"> </div></div>
							<p class="section-2-p">
								<span class="text-dollar">$</span> <span class="section-2-revenue"> 32.00 </span>
							</p>
							<button type="button" class="stripe-button-el" id="smooth-scroll-button-1" style="visibility: visible;">
								<span style="display: block; min-height: 30px;">Add yourself to waiting list</span>
							</button>
							<!--</form>-->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- SECTION 3 -->
	<div class="container section-3">
		<div>
			<div class="row">
				<div class="col-md-12 text-center butwhy" >
				<h1 class="butwhy">Beyond numbers... is experience</h1></div>
			</div>
		</div>
		<div class="split_screen">
		<div>
			<div class="row desktop">
				<div class="col-md-4 for-you">
					<h1 class="blueboldtext">For you <img src="assets/images/icon-you.png"/></h1>
					<div class="row">
						<div class="col-md-12 circle1">
							<h3 class="upperh3">Generate more ad revenue</h3>
							<p class="pgap">Mobile advertising revenue now makes up 54% of all digital revenue</p>
						</div>
						<div class="col-md-12 circle2">
							<h3 class="upperh3">Go Omnipresent</h3>
							<p class="pgap">In office or home,who cares. Be with your users all the time.</p>
						</div>
						<div class="col-md-12 circle3">
							<h3 class="upperh3">Notify users on the go</h3>
							<p class="pgap">Provide instant notification to your users on the go</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div>
						<img  src="assets/images/circle-mobile-visual.png" class="icwm" />
						</div>
				</div>
				<div class="col-md-4 for-your-user">
					<h1 class="blueboldtext2"><img src="assets/images/icon-clarity-point1.png" class="for-your-user1" /> For your user</h1>
					<div class="row">
						<div class="col-md-12 circle1r">
							<h3 class="upperh3">Read.Anywhere.Anytime </h3>
							<p class="pgap">Now let your users read blogs without getting limited to phycial space.</p>
						</div>
						<div class="col-md-12 circle2r">
							<h3 class="upperh3">Read.Pause.Resume</h3>
							<p class="pgap">With the option to bookmark your blog,resume from where you have stopped.</p>
						</div>
						<div class="col-md-12 circle3r">
							<h3 class="upperh3">Follow your favourite blogger</h3>
							<p class="pgap">Stay updated about your fellow blogger</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row mobile">
				<div class="mobile__block_1 clearfix">
					<div class="half-img-1">
						<img src="assets/images/circle-mobile-visual-half-1.png" />
					</div>
					<div class="block__content">
						<h1 class="blueboldtext">For you <img src="assets/images/icon-you.png"/></h1>
						<div class="">
							<div class="circle1">
								<h3 class="upperh3">Generate more ad revenue</h3>
									<p class="pgap">Mobile advertising revenue now makes up 54% of all digital revenue</p>
							</div>
							<div class="circle2">
								<h3 class="upperh3">Go Omnipresent</h3>
								<p class="pgap">In office or home,who cares. Be with your users all the time.</p>
							</div>
							<div class="circle3">
								<h3 class="upperh3">Notify users on the go</h3>
								<p class="pgap">Provide instant notification to your users on the go</p>
							</div>
						</div>
					</div>
				</div>
					<div class="mobile__block_2 clearfix">
						<div class="half-img-2">
							<img src="assets/images/circle-mobile-visual-half-2.png" />
						</div>
					<div class="block__content">
						<h1 class="blueboldtext2"><img src="assets/images/icon-clarity-point1.png" class="for-your-user" /> For your user</h1>
						<div class="row">
							<div class="col-md-12 circle1r">
								<h3 class="upperh3">Read.Anywhere.Anytime </h3>
								<p class="pgap">Now let your users read blogs without getting limited to phycial space.</p>
							</div>
							<div class="col-md-12 circle2r">
								<h3 class="upperh3">Read.Pause.Resume</h3>
								<p class="pgap">With the option to bookmark your blog,resume from where you have stopped.</p>
							</div>
							<div class="col-md-12 circle3r">
								<h3 class="upperh3">Follow your favourite blogger</h3>
								<p class="pgap">Stay updated about your fellow blogger</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	<!--SECTION 4 -->

	<div class="container-fluid section-4">
		<div class="container">
			<div class="row text-center">
				<p class="moreclarity"> Clarity much? </p>
			</div>

			<div class="row row1">
				<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1">
					<img src="assets/images/icon-clarity-point1.png" alt="Icon User" class="row1-img" />
				</div>
				<div class="col-md-7 row1-text ">
					<span class="text-clarity-point">You will not lose on your existing users. But enable them with all features in a
					mobile application. </span>
				</div>
			</div>
			<div class="row row2">
				<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-right row2-text">
					<span class="text-clarity-point">Retain your design branding or a get fresh new one designed just for you. </span>
				</div>
				<div class="col-md-1 col-sm-1 r2-image">
					<img src="assets/images/icon-clarity-point2.png" alt="Icon User" class="row2-img"/>
				</div>
			</div>
			<div class="row row3">
				<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 r3-image">
					<img src="assets/images/icon-clarity-point3.png" alt="Icon User" class="row3-img"/>
				</div>
				<div class="col-md-7 row3-text">
					<span class="text-clarity-point">Convert your WordPress blogging website to a mobile application will be much cheaper than
					building an app from scratch. </span>
				</div>
			</div>
		</div>
	</div>

	<!-- SECTION 6 -->
	<div class="bg-ground container-fluid">

		<div class="bg-ground-1">
		<div class="row text-center">
			<div class="col-md-12 col1" >
				<div class="div-text">
					<h1 class="wtext1">Going mobile is Inevitable &amp; &nbsp;Profitable</h1>
					<h3 class="wtext2">There is no doubt that you will go to mobie platform. The only question is when?</br> And we believe the sooner , the better.</h3>
					<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-center video-section-form">
						<div class="col-md-7 col-sm-7">
							<input type="text" placeholder="Provide your email" id="video-email1"/>
						</div>
						<div class="col-md-5 col-sm-5">
							<button type="button" class="btn btn-primary" onclick="apiCall1()">Join the Waitlist! </button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="page-footer font-small blue pt-4 mt-4">
			<div class="container-fluid text-center text-md-left">
				<div class="row">


					<div class="col-md-12 fottrtext">
						<h5 class="text-uppercase fottersize">Copyright 2018.CTRL.BIZ <!--&nbsp;&nbsp; | &nbsp;&nbsp; Privacy Policy &nbsp;&nbsp; | &nbsp;&nbsp;  Terms & Conditions--></h5>
					</div>

				</div>
			</div>
		</footer>
		</div>
	</div>

	<!---MODAL -->
	<!-- AGILE CRM -->

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<img src="assets/images/Bitmap.png" alt="Logo" class="modal-logo" /><br/><br/><br/><br/>
				<p class="modal-header-p"> Get your app for $99/mo </p>
			</div>
			<div class="modal-body">
				<form method="POST" class="modal-form">
					 <div>
						<span class="glyphicon glyphicon-user modal-icon"><input type="text" placeholder="Name" id="name" required /></span>
					</div>
					<br/><br/>
					<div>
						<span class="glyphicon glyphicon-envelope modal-icon"><input type="email" placeholder="Email" id="email" required /></span>
					</div>
					<br/><br/>
					<div>
						<span class="glyphicon glyphicon-phone modal-icon"><input type="tel" placeholder="Contact No." id="contact" required /></span>
					</div>
					<div>
					<br/><br/>
						<span class="glyphicon glyphicon-home modal-icon"><input type="text" placeholder="Company Name" id="company" /></span>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="paypal-footer text-center">
					<button type="button" alt="Paypal" onclick="apiCall(this)" data-toggle="modal" data-target="#myPayment"> PROCEED</button>
				</div>
			</div>
			</div>
		</div>
	</div>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117121595-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-117121595-1');
	</script>
</body>
</html>
