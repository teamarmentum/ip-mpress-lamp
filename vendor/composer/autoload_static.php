<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit10ecac662f1f50e0d3713e260cbefaf0
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit10ecac662f1f50e0d3713e260cbefaf0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit10ecac662f1f50e0d3713e260cbefaf0::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
